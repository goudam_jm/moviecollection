package com.mak.moviecollection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.graphics.Color;

import java.util.Objects;


public class MovieCollection extends AppCompatActivity {

    //used as a key in key value pair that's passed between activitites
    public static final String ROW_ID = "row_id";

    private ListView movieListView;

    //adapter for populating the list view
    private CursorAdapter movieAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_collection);
        movieListView = (ListView)findViewById(R.id.listView);
        movieListView.setOnItemClickListener(viewMovieListener);
        movieListView.setBackgroundColor(Color.BLACK);

        //display message on empty list
        TextView emptyText = (TextView) View.inflate(this,R.layout.movie_list_empty_item, null);
        emptyText.setVisibility(View.GONE);

        ((ViewGroup)movieListView.getParent()).addView(emptyText);

        //map each movie's name to a text view on the listview layout
        String[] from = new String[] {"title"};
        int[] to = new int[]{R.id.movieTextView};
        movieAdapter = new SimpleCursorAdapter(MovieCollection.this,
                R.layout.movie_list_item,null,from,to,0);

        //movie adapter to listview to display data
        movieListView.setAdapter(movieAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_movie_collection, menu);
        return true;



    }

    @Override
    protected void onResume(){
        super.onResume();
        //create a new GetMoviesTask and execute it
        //on background thread gets the complete list of all rows and
        //populates the list view

        new GetMoviesTask().execute((Object[])null);
    }

    //performs database query outside of GUI thread
    private class GetMoviesTask extends AsyncTask<Object,Object,Cursor>{
        DatabaseConnector databaseConnector = new DatabaseConnector(MovieCollection.this);

        //perform database access
        @Override
        protected Cursor doInBackground(Object... params){
            databaseConnector.open();
            return databaseConnector.getAllMovies();
        }
        //use the cursor return from doInBackground
        //runs on the UI thread
        @Override
        protected void onPostExecute(Cursor result){
            movieAdapter.changeCursor(result);
            databaseConnector.close();

        }
    }

    @Override

    protected void onStop(){
        Cursor cursor = movieAdapter.getCursor();
        //if not null deactivate it
        if(cursor != null){
            cursor.close();
        }
        movieAdapter.changeCursor(null);
        super.onStop();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //create a new intent to launch AddEditMovieActivity
        Intent addNewMovie =
                new Intent(MovieCollection.this,AddEditMovie.class);
        startActivity(addNewMovie);


        return super.onOptionsItemSelected(item);
    }

    OnItemClickListener viewMovieListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //create an Intent to launch the ViewMovieActivity
            Intent viewMovie =
                    new Intent(MovieCollection.this,ViewMovie.class);
            //pass the selected movies' row ID as an extra to the intent
            //adv is key values pairs
            viewMovie.putExtra(ROW_ID,id);
            startActivity(viewMovie);


        }
    };
}
